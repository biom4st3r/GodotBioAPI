# Defaults
## Dialog
* A basic hackable dialog system that popups up an interactible and configurable text box
* Check Data/dialog.log for config details
* Easliy invoked via Interactable System
* `God.open_dialog("KEY_FROM_dialog.json");`
* When adding Requirements or DialogTypes consider updating DialogBuilder

## Interaction
* Basic system for the player to interact with objects
* interface details located @ `God.Interact`
* Used Collision layer 31 by default
* Two modes:
 - Ray - Raycasts forward to interact with whatever is in front of the player
```
elif Input.is_action_just_pressed("interact"):
	God.Interact.try_interact(self,raycast);
```
 - Field - Creates a scrollable gui to interact with anything in a collision shape around the player
```
def _ready():
    $InteractionField.player = self;
    interaction_field.body_entered.connect(func(body): $InteractionField.add_entry(body));
    interaction_field.body_exited.connect(func(body): $InteractionField.remove_entry(body));
```

## Inventory
* Basic Inventory gui w/ Drag, Drop, and Swap
* `God.open_inventory()`
* Depends on: PlayerData

## Quests
* Basic quest gui/system
* `God.open_quest(...)`
* Start, complete, accept, reject, and stopping quests
* Configurable only in code. Direct add to Quests._quests
* `God.open_quest_menu()`
* `God.open_quest(id, true)`
* Depends on: PlayerData

## Shop
* Simple configurable shop gui
* Can be opened as Player buying or selling
* Filterable selling(Check dialog.json)
* `God.open_shop(data)`
* Depends on: PlayerData

## Toast
* Pops up Tooltip/toasts on the top right of the screen
* `ToastManager.add_toast(null, "Tihs is a message")`
* No Dependencies

class_name ToastManager extends Control

static var instance: ToastManager = self;
@onready var container: VBoxContainer = $VBoxContainer;
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if instance != null:
		printerr("MULTIPLE ToastManagers Detected!!!!!");
		instance.queue_free();
	instance = self;

static func add_3d_toast(icon: Node3D, text: String) -> void:
	var toast: Control = preload("./toast3d.tscn").instantiate();
	toast.init(icon, text);
	ToastManager.instance.container.add_child(toast);

static func add_toast(icon: Texture2D, text: String) -> void:
	var toast: Control = preload("./toast.tscn").instantiate();
	var hbox = toast.get_child(0).get_child(0)
	(hbox.get_child(0) as TextureRect).texture = icon;
	(hbox.get_child(1) as Label).text = text;
	ToastManager.instance.container.add_child(toast);

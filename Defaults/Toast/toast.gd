extends PanelContainer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$AnimationPlayer.play("Fadeout");
	$AnimationPlayer.animation_finished.connect(func(name): self.queue_free());

func init(node: Node3D, text: String) -> void:
	$MarginContainer/HBoxContainer/SubViewport/Node3D.add_child(node);
	node.position = Vector3.ZERO;
	$MarginContainer/HBoxContainer/Label.text = text;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


extends Control

const ENTRY = preload("./entry.tscn");
const Type = God.InteractionType;
@onready var list = $ScrollContainer/VBoxContainer;
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	God.remove_all_children(list);
	God.disable_node(self);
	pass # Replace with function body.

var entries: Array[Control] = []
var keys: Array[Node3D] = []
var selection: int = 0;
var mutex = Mutex.new();
var player;

func _init_entry(entry: Control, icon: Texture2D, text: String):
	var eicon: TextureRect = entry.get_child(0).get_child(0);
	eicon.texture = icon;
	var etext: Label = entry.get_child(0).get_child(1);
	etext.text = text;
	_select_entry(entry, false);
	
func _select_entry(entry: Control, selected: bool):
	entry.modulate = Color(1,1,1,1) if selected else Color(1,1,1,0.3);

func refresh_selection():
	if entries.size() == 0:
		return;
	# TODO Probably mutex
	for x in entries:
		_select_entry(x, false);
	if selection >= entries.size():
		selection = 0;
	_select_entry(entries[selection], true);

func update_selection(i: int):
	# TODO Probably mutex
	_select_entry(entries[self.selection], false);
	self.selection = i;
	_select_entry(entries[self.selection], true);

func add_entry(obj: Node3D):
	# TODO: Sort by distance to player
	if keys.size() == 0:
		God.enable_node(self);
	var it: Type = God.Interact.get_type(obj);
	var entry = ENTRY.instantiate();
	match it:
		Type.Default:
			_init_entry(entry, null, "Interact With");
		Type.Dialog:
			_init_entry(entry, null, "Talk to");
		Type.Item:
			_init_entry(entry, God.Interact.get_item(obj).get_icon(), "Pickup");
		_:
			printerr("Interaction Field: InteractionType not handled %s" % [it])
	mutex.lock();
	keys.insert(0, obj);
	entries.insert(0, entry);
	#keys.append(obj);
	#entries.append(entry);
	mutex.unlock();
	list.add_child(entry);
	list.move_child(entry, 0);
	refresh_selection();

func remove_entry(obj: Node3D):
	mutex.lock();
	var index = keys.find(obj);
	keys.pop_at(index);
	var e = entries.pop_at(index);
	mutex.unlock();
	list.remove_child(e);
	e.queue_free();
	refresh_selection();
	if keys.size() == 0:
		God.disable_node(self);

func _input(event: InputEvent) -> void:
	if God.is_scroll_up(event):
		if self.selection == 0:
			update_selection(self.keys.size() - 1);
		else:
			update_selection(self.selection - 1);
	elif God.is_scroll_down(event):
		update_selection((self.selection + 1) % self.keys.size());
	elif Input.is_action_just_pressed("interact"):
		God.Interact.interact_with(keys[self.selection], self.player);
	else:
		return;
	self.get_viewport().set_input_as_handled();


extends NinePatchRect

func _take_control(node: Object, data: Variant):
	if node != God.GuiKeys.DIALOG_BOX:
		return;
	God.release_mouse();
	self.visible = true;
	set_process_input(true);
	set_process(true);
	_start_dialog(data as Array);

func _ready():
	set_process_input(false);
	set_process(false);
	God.instance.take_control.connect(_take_control);
	#God.instance.init_dialog.connect(_start_dialog);
	self.visible = false;

var dialog_data: Array = [];
var current_dialog: int = 0;
var dialog_type: DialogType = DialogType.Undefined; # Null

# Takes in the whole of the DialogArray and Initilizes the dialog gui
func _start_dialog(dialog_data: Array):
	self.dialog_data = dialog_data;
	self.current_dialog = 0;
	self.visible = true;
	load_dialog();

func _end_dialog():
	# Save memory or something.
	self.dialog_data = [];
	# Stop listening
	set_process_input(false);
	set_process(false);
	# uninit
	self.dialog_type = DialogType.Undefined;
	self.current_dialog = -1;
	self.visible = false;
	# Return control to player
	God.return_control_to_player();

enum DialogType {
	Undefined,
	Text,
	Options,
	Start_Quest,
	Update_Quest,
	Shop,
};

func load_dialog():
	var dialog: Dictionary;
	# load data from new index and check for requirements until met
	while true:
		if self.current_dialog == -1:
			_end_dialog();
			return;
		if self.current_dialog >= dialog_data.size():
			printerr("ran out of dialog");
			_end_dialog();
			return;
		dialog = dialog_data[self.current_dialog] as Dictionary;
		self.dialog_type = DialogType.get(dialog.dtype, DialogType.Undefined);
		if 'requires' in dialog and !meets_requirements(dialog.requires):
			self.current_dialog+=1;
			continue;
		break;

	# Hide all types
	for x in self.get_children():
		x.visible = false;
	match self.dialog_type:
		DialogType.Text:
			_start_dialog_Text(dialog);
		DialogType.Options:
			_start_dialog_Options(dialog);
		DialogType.Start_Quest:
			_start_dialog_StartQuest(dialog);
		DialogType.Update_Quest:
			_start_dialog_UpdateQuest(dialog);
		DialogType.Shop:
			_open_shop(dialog);
		_:
			printerr("Unhandled dialog type ", dialog);
			_end_dialog();

func advance_dialog(goto):
	# Missing goot. end dialog
	if goto == null:
		current_dialog = -1;
		return
	# number goto. goto next or index
	elif goto is int or goto is float:
		if goto == -1:
			current_dialog += 1;
		else:
			current_dialog = goto as int;
		return
	# string goto. jump to label
	else:
		for x in dialog_data.size():
			if dialog_data[x].get('label', null) == goto:
				current_dialog = x;
				return;
				break;
	printerr("failed to advance dialog: goto: ", goto);

func clean_dialog_Options():
	for x in $Options/MarginContainer/VBox.get_children():
		x.queue_free();

const clickable_color = 0xa8ffffff;
const hover_color = 0x008586ff;

#enum Operators{ all, any, negate, has_quest, has_item };

#class Requires:
#	var id: Operators,
#	var data;
#
func meets_requirements(obj: Dictionary) -> bool:
	match obj.id:
		"any":
			var f: bool = false;
			for x in obj.data:
				f = f || meets_requirements(x);
				if f:
					return true;
		"all":
			var f: bool = false;
			for x in obj.data:
				f = f && meets_requirements(x);
				if !f:
					return false;
			return true;
		"negate":
			return !meets_requirements(obj.data)
		"has_quest":
			match obj.data.state:
				'started':
					for x in God.data.active_quests:
						if x.quest_id == obj.data.quest_id:
							return true;
				'finished':
					for x in God.data.completed_quests:
						if x.quest_id == obj.data.quest_id:
							return true;
		"has_item":
			for x in God.data.inventory:
				if String(x.item.id) == obj.data.item:
					if x.count <= obj.data.count:
						return true;
		_:
			printerr("unhandled requirements operator ", obj);
			return false;
	return false;

static func dataToItemStack(data: Dictionary):
	var s = ItemStack.new(God.get_item(data.item), data.count);
	s.data = data.data;
	return s;

func _open_shop(dialog_step: Dictionary):
	# TODO: This isn't a good interface.
	# What about npcs with a persistant inventory?
	# Maybe close_shop signal with the resulting inventory?
	# Check shop.gd for implementation
	_end_dialog();
	
	var settings: Dictionary = dialog_step.ddata.duplicate();
	var inv: Array[ItemStack] = [];
	for x in settings.inventory:
		inv.append(dataToItemStack(x));
	settings.inventory = inv;

	God.open_shop(settings);

func _start_dialog_StartQuest(dialog_step: Dictionary):
	_end_dialog();
	God.quests.try_start_quest(dialog_step.ddata, true);

func _start_dialog_UpdateQuest(dialog_step: Dictionary):
	var finished: bool = God.data.update_quest(dialog_step.ddata);
	advance_dialog(dialog_step.on_finish_goto if finished else dialog_step.goto);
	clean_dialog_Options();
	load_dialog();

#class OptionsEntry:
#	ddata: Array[OptionSettings]

#class OptionSettings:
#	text: String;
#	goto: int;
#	color: HexString
#	hover_color: HexString
#	requires: RequiredParsable

func _start_dialog_Options(dialog_step: Dictionary):
	clean_dialog_Options();
	$Options.visible = true;
	var ddata: Array = dialog_step.ddata;
	for x in ddata:
		if 'requires' in x and !meets_requirements(x.requires):
			continue;
		var label: Label = $OptionsLabel.duplicate();
		label.text = x.text;
		label.visible = true;
		label.label_settings = label.label_settings.duplicate();
		$Options/MarginContainer/VBox.add_child(label);
		label.mouse_entered.connect(func():
			print("mouse_entered ", x);
			label.label_settings.font_color = hover_color;
		)
		label.mouse_exited.connect(func():
			print("mouse_exited ", x);
			label.label_settings.font_color = clickable_color;
		)
		label.gui_input.connect(func(event: InputEvent):
			if event is InputEventMouseButton:
				if (not event.is_pressed()) and event.button_index == MOUSE_BUTTON_LEFT:
					advance_dialog(x.get('goto', null))
					clean_dialog_Options();
					load_dialog();
		)


#class TextEntry:
#	ddata: String;
#	goto: int;
#
#
func _start_dialog_Text(dialog_step: Dictionary):
	$GeneralText.visible = true;
	$GeneralText/Label.visible_characters = 0;
	$GeneralText/Label.text = dialog_step.ddata;
	advance_dialog(dialog_step.get('goto', null));

func _input(event: InputEvent):
	if event is InputEventMouse:
		return;
	if not event.is_pressed():
		return;
	match self.dialog_type:
		DialogType.Text:
			print("visible: ", $GeneralText/Label.visible_characters, " | ", "total: ", $GeneralText/Label.text.length())
			if $GeneralText/Label.visible_characters < $GeneralText/Label.text.length():
				self.visible_chars = $GeneralText/Label.text.length()
			else:
				$GeneralText/Label.visible_characters = 0;
				self.visible_chars = 0;
				load_dialog();

func _process(delta):
	if self.visible and dialog_data.size() != 0:
		_tick_dialog(delta);

var visible_chars: float = 0;

func _tick_dialog(delta):
	match self.dialog_type:
		DialogType.Text:
			visible_chars += God.TEXT_SPEED * delta;
			$GeneralText/Label.visible_characters = floori(visible_chars);

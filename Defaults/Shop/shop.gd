extends PanelContainer

@onready var ICON: TextureRect = $LeftAndRight/Left/Bg/MarginContainer/Icon;
@onready var LIST: Control = $LeftAndRight/List/MarginContainer/ItemContainer;
const Entry = preload("./entry.tscn");

var limited: bool = false;
var multiplier: float = 1.0;
var player_buying: bool = false;
var inventory: Array[ItemStack];

# Only used for player selling
var player_inv_index: Array[int];
var entries: Array[Control];

var index: int = 0;

func _close():
	reset();
	self.visible = false;
	God.disable_node(self);
	God.return_control_to_player();

func reset():
	self.inventory = [];
	self.entries = [];
	self.player_inv_index = [];
	self.index = 0;
	self.limited = false;
	self.player_buying = false;
	self.multiplier = 1.0;

func update_entry(entry: Control, stack: ItemStack, active: bool):
	var flat: StyleBoxFlat = entry.get("theme_override_styles/panel");
	flat.bg_color = Color(0,0,0,1) if active else Color(0,0,0,0);

	entry.get_child(0).get_child(0).text = "%-19s $%d" % [stack.get_name(), stack.get_price()];
	if active:
		ICON.texture = stack.get_icon();

func _setup_player_selling(settings: Dictionary):
	var player: Array[ItemStack] = God.data.inventory;
	var inv: Array[ItemStack] = settings.inventory;
	if !inv.is_empty(): # Will only buy what is in the inventory
		var items = []
		for x in inv:
			items.append(x.item);
		# Filter out what's not in the shop buyable inventory
		for x in player.size():
			if player[x].get_price() == 0:
				continue;
			if player[x].item in items:
				# Store the itemstack and index
				self.inventory.append(player[x].copy());
				self.player_inv_index.append(x);
	else:
		for x in player.size():
			if player[x].get_price() == 0:
				continue;
			self.inventory.append(player[x].copy())
			self.player_inv_index.append(x)
	# Shop Inventory is setup
	# Make the entries
	for stack in self.inventory:
		var entry: PanelContainer = Entry.instantiate();
		update_entry(entry, stack, false);
		LIST.add_child(entry);
		entries.append(entry);

func _setup_player_buying(settings: Dictionary):
	var inv: Array[ItemStack] = [];
	for stack in settings.inventory:
		if stack.get_price() != 0:
			inv.append(stack);
	self.inventory = inv;
	for stack in inv:
		var entry: PanelContainer = Entry.instantiate() as PanelContainer;
		update_entry(entry, stack, false);
		LIST.add_child(entry);
		entries.append(entry);

func _setup(settings: Dictionary):
	reset();
	God.remove_all_children(LIST);
	self.limited = settings.limited;
	self.multiplier = settings.multiplier;
	self.player_buying = settings.player_buying;
	
	if self.player_buying:
		_setup_player_buying(settings);
	else:
		_setup_player_selling(settings);
	
	if entries.size() == 0:
		printerr("TODO No items in shop");
		God.return_control_to_player();
	else:
		update_entry(entries[0], inventory[0], true);

func _take_control(obj: Object, settings: Variant):
	if obj != God.GuiKeys.SHOP:
		return;
	_setup(settings as Dictionary);
	God.enable_node(self);
	self.visible = true;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	God.instance.take_control.connect(_take_control);
	self.visible = false;
	God.disable_node(self);

func _accept_buying():
	if limited:
		printerr("TODO Implement limited inventory");
	pass
	
func _remove_at_index():
	self.inventory.pop_at(self.index);
	self.player_inv_index.pop_at(self.index);
	self.entries.pop_at(self.index).queue_free();
	if self.index >= self.inventory.size():
		self.index -= 1;
	if self.inventory.size() == 0:
		printerr("TODO Handle empty selling inventory");
		self._close();
		return;
	update_entry(self.entries[self.index], self.inventory[self.index], true);

func _accept_selling():
	var stack = self.inventory[self.index];
	var price = stack.get_price();
	var inv_index = self.player_inv_index[self.index];
	stack.count -= 1;
	# Remove item
	God.data.increment_slot(inv_index, -1);
	# Give coins
	God.data.add_item(ItemStack.new(God.get_item("coins"), price));
	if stack.count <= 0:
		_remove_at_index();

func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		if player_buying:
			ToastManager.add_toast(self.inventory[self.index].get_icon(), "You bought x1 %s" % [self.inventory[self.index].get_name()]);
			_accept_buying();
		else:
			ToastManager.add_toast(self.inventory[self.index].get_icon(), "You sold x1 %s" % [self.inventory[self.index].get_name()]);
			_accept_selling();
	if Input.is_action_just_pressed("ui_cancel"):
		self._close();
	elif Input.is_action_just_pressed("ui_up"):
		self.update_entry(self.entries[self.index],self.inventory[self.index], false);
		if self.index == 0:
			self.index = self.inventory.size() - 1;
		else:
			self.index -= 1;
		self.update_entry(self.entries[self.index],self.inventory[self.index], true);
	elif Input.is_action_just_pressed("ui_down"):
		self.update_entry(self.entries[self.index],self.inventory[self.index], false);
		self.index += 1;
		self.index %= self.inventory.size();
		self.update_entry(self.entries[self.index],self.inventory[self.index], true);
	else:
		return;
	self.get_viewport().set_input_as_handled();


extends StaticBody3D

@export var buying: bool = true;

func __interact_with(_player):
	if buying:
		God.open_dialog("sample_shop")
	else:
		God.open_dialog("sample_sell_shop");

func __interact_type():
	return God.InteractionType.Dialog;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	pass


extends PanelContainer

@onready var NAME = $Details/MarginContainer/HBoxContainer/CenteredLabel/Label;
@onready var ICON = $Details/MarginContainer/HBoxContainer/CenteredIcon/TextureRect;
@onready var HELD_ITEM_COUNT: Label = $HeldItem/Label;
@onready var HELD_ITEM_TEXTURE: TextureRect = $HeldItem/TextureRect;

const INVALID_SLOT: int = -1;

var SLOTS: Array[TextureRect] = []

var clicked_slot: int = INVALID_SLOT;
var hovered_slot: int = INVALID_SLOT;

var mouse_pos: Vector2 = Vector2.ZERO;
var held_item = null;
var inventory: Array[ItemStack];

func _take_control(node: Object, nothing: Variant):
	if node != God.GuiKeys.INVENTORY_MENU:
		return;
	inventory = God.data.inventory;
	init_slots();
	God.release_mouse();
	self.visible = true;
	God.enable_node(self);

func return_held_item():
	update_slot(clicked_slot, held_item);
	held_item = null;
	end_drag();

func swap_held_and_hovered():
	var stack = inventory[hovered_slot].copy();
	update_slot(hovered_slot, held_item);
	held_item = null;
	update_slot(clicked_slot,stack);
	end_drag();
	pass

func left(slot: int, pressed: bool):
	if pressed:
		var stack := inventory[slot].copy();
		if stack.is_empty():
			return;
		NAME.text = stack.get_name() + "(" + (stack.item.id if stack.is_empty() else stack.item.id) + ")";
		ICON.texture = stack.get_icon();
		clicked_slot = slot;
	elif held_item:
		if hovered_slot == INVALID_SLOT:
			return_held_item();
			print('returned held_item to ', clicked_slot, ' | ', slot, ' was full');
		elif inventory[hovered_slot].is_empty():
			update_slot(hovered_slot, held_item);
			held_item = null;
			# visual_update_slot(hovered_slot)
			print('moved held_item to ', hovered_slot);
			end_drag();
		else:
			swap_held_and_hovered();
	else: # not pressed. Not held_item
		
		clicked_slot = -1;


#func _get_drag_data(at_position):
#	pass
#
#func _can_drop_data(at_position, data):
#	pass

func right(inventoryidx: int, pressed: bool):
	pass

func middle(inventoryidx: int, pressed: bool):
	pass

func scroll_up(inventoryidx: int, pressed: bool):
	pass

func scroll_down(inventoryidx: int, pressed: bool):
	pass

func _slot_gui_input(index: int, input: InputEvent):
	if input is InputEventMouseButton:
		var press: bool = input.pressed;
		match input.button_index:
			1:
				left(index, press);
			2:
				right(index, press);
			3:
				middle(index, press);
			4:
				scroll_up(index, press);
			5:
				scroll_down(index, press);
			_:
				print(index, ' ', input);

func _ready():
	God.instance.take_control.connect(_take_control);
	var i = 0;
	for x in $Slots.get_children():
		var index = i; # Worried about passing reference
		var slot := x as ColorRect;# as TextureRect;
		slot.gui_input.connect(func(event):
			_slot_gui_input(index, event);
		);
		slot.mouse_entered.connect(func():
			# print("mouse_entered ", index);
			hovered_slot = index;
		);
		slot.mouse_exited.connect(func():
			# print("mouse_exited ", index);
			hovered_slot = INVALID_SLOT;
		);
		SLOTS.append(x.get_node("TextureRect"));
		i += 1;
	self.visible = false;
	$HeldItem.visible = false;
	God.disable_node(self);

func visual_update_slot(x: int):
	SLOTS[x].get_parent().visible = true;
	SLOTS[x].texture = null if inventory[x].is_empty() else inventory[x].get_icon();
	(SLOTS[x].get_parent().get_node("Label") as Label).text = str("" if inventory[x].is_empty() else str(inventory[x].count));

func update_slot(x: int, item: ItemStack):
	inventory[x] = item;
	God.data.update_inventory_slot(x,item);
	visual_update_slot(x);

func init_slots():
	for x in SLOTS.size():
		if x < inventory.size():
			visual_update_slot(x);
		else:
			SLOTS[x].get_parent().visible = false;

func clear_items():
	NAME.text = "";
	ICON.texture = null;
	for x in SLOTS:
		x.texture = null;

func start_drag():
	held_item = inventory[clicked_slot];
#	inventory[clicked_slot] = {};
	update_slot(clicked_slot, ItemStack.EmptyStack);

	visual_update_slot(clicked_slot);
	HELD_ITEM_COUNT.text = str(held_item.count);
	HELD_ITEM_TEXTURE.texture = held_item.get_icon();
	print('start_drag');

func end_drag():
	if held_item:
		printerr("Held Item was not removed before end_drag");
	clicked_slot = INVALID_SLOT;
	mouse_pos = Vector2.ZERO;
	$HeldItem.visible = false;
	# print('end_drag');

func _unhandled_input(event: InputEvent) -> void:
	if not self.visible:
		return;
	if Input.is_action_just_pressed("inventory"):
		get_viewport().set_input_as_handled();
		if held_item:
			God.data.drop_item(held_item);
			held_item = null;
		end_drag();
		print('inventory realease');
		God.return_control_to_player();
		self.visible = false;
		clear_items();
		set_process_input(false);
		return;

func _input(event: InputEvent):
	if event is InputEventMouse and clicked_slot > -1:
		# if Position is uninitilized
		if mouse_pos == Vector2.ZERO:
			mouse_pos = event.position;
		if held_item != null:
			$HeldItem.position = event.position - Vector2(250,120);
			$HeldItem.visible = true;

		# Make sure start_drag is only called once by relying on held_item
		elif mouse_pos.distance_to(event.position) > 20:
			start_drag();
		pass


class_name ItemStack extends RefCounted


static var MissingItem: Variant = {};
static var EmptyStack: ItemStack = ItemStack.new(MissingItem, 0);
var item: Variant;
var count: int;
var data: Dictionary = {};

func _init(_item: Variant, _count: int, data = null):
	self.item = _item;
	self.count = _count;
	if data:
		self.data = data as Dictionary;

func _to_string() -> String:
	return item.id + ":" + str(self.count);

#static func parse(dict: Dictionary) -> ItemStack:
	#if dict.is_empty():
		#return Items.EMPTY_STACK;
	#var stack := new(Items.items.get(dict["id"], Items.MISSING_ITEM), dict["count"]);
	#return stack;

func copy() -> ItemStack:
	if self.is_empty():
		return ItemStack.EmptyStack;
	var stack = ItemStack.new(self.item, self.count, self.data.duplicate());
	return stack;

#func to_immutable() -> ImmutableItemStack:
	#return ImmutableItemStack.new(self);

func is_empty() -> bool:
	if self.item is Object:
		return self.count < 1;
	else:
		return self.item == ItemStack.MissingItem or self.count < 1;

func get_name() -> String:
	if 'item_name' in self.item:
		return item.item_name;
	return item.to_string();

func get_icon() -> Texture2D:
	if 'icon' in self.item:
		return self.item.icon;
	return preload("../../paper.png")

func get_price() -> int:
	if 'price' in self.item:
		return self.item.price;
	return 0;

func left_click(raycast: RayCast3D, player) -> void:
	if 'left_click' in self.item:
		print("Found left click");
		self.item.left_click(self, raycast, player);
	pass

func right_click(raycast: RayCast3D, player) -> void:
	if 'right_click' in self.item:
		print("Found left click");
		self.item.right_click(self, raycast, player);
	pass
func tick(player) -> void:
	if 'tick' in self.item:
		self.item.tick(self, player);
	pass
func input(event: InputEvent, player) -> void:
	if 'input' in self.item:
		self.item.input(self, event, player);
	pass
func selected() -> void:
	if 'on_selection' in self.item:
		self.item.on_selection(self);
	pass
func unselect() -> void:
	if 'unselect' in self.item:
		self.item.unselect(self);
	pass
static func matches(a: ItemStack, b: ItemStack) -> bool:
	if a.is_empty() and b.is_empty():
		return true
	if typeof(a.item) != typeof(b.item):
		return false;
	return a.item == b.item and a.data == b.data;
#func as_world_item() -> Node3D:
	#if Items.ITEM_WORLD_ITEM_PROVIDER in self.item.data:
		#return self.item.data[Items.ITEM_WORLD_ITEM_PROVIDER].call(self);
	#var wi = preload("res://scenes/WorldItem.tscn");
	#wi.stack = self.copy();
	#return wi;

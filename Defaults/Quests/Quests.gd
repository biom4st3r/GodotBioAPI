class_name Quests extends Node



var quests: Dictionary = {
	"empty_quest": EmptyQuest.new(),
	"sample_fetch_quest_0": FetchQuest.new("Fetch: 5 Seeds", "Go find 5 seeds for the yellow npc", ItemStack.new(God.get_item("purple_test_seed"), 5)),
};

func try_start_quest(quest_id: String, show_buttons: bool = false) -> void:
	God.open_quest(quest_id, show_buttons);

func _get_quest(quest_id: String) -> Quest:
	return quests.get(quest_id, quests['empty_quest']);

class QuestEntry:
	var quest_id: String;
	var data: Dictionary;
	func _init(quest_id: String, data: Dictionary = {}):
		self.quest_id = quest_id;
		self.data = data;

class Quest:
	var title: String;
	var body: String;
	func _init(title: String, body: String):
		self.title = title;
		self.body = body;
	# If the player can start this quest. null means no
	func _can_start(quest_id: String) -> QuestEntry:
		return null;

	func _update_state(entry: QuestEntry) -> void:
		pass

	func _is_complete(entry: QuestEntry) -> bool:
		return false;

	func _get_additional_elements(entry: QuestEntry) -> Array[Control]:
		return [];

class FetchQuest extends Quest:
	var goal: ItemStack;
	func _init(title: String, body: String, stack: ItemStack):
		super._init(title,body);
		self.goal = stack.copy()

	func _can_start(quest_id: String) -> QuestEntry:
		var entry: QuestEntry = QuestEntry.new(quest_id, {});
		entry.data['count'] = 0;
		return entry;

	func _remaining(entry: QuestEntry) -> int:
		return goal.__item_stack.count - entry.data.count;

	func _update_state(entry: QuestEntry) -> void:
		var amount: int = _remaining(entry);
		if amount < 1: return;
		for x in God.data.inventory.size():
			var stack: ItemStack = God.data.inventory[x];
			if stack.item == goal.__item_stack.item:
				amount = mini(amount, stack.count);
				God.data.increment_slot(x, -amount);
				entry.data.count += amount;
				break;

	func _is_complete(entry: QuestEntry) -> bool:
		return entry.data.count >= goal.__item_stack.count;

	func _get_additional_elements(entry: QuestEntry) -> Array[Control]:
		return [];

class EmptyQuest extends Quest:
	func _init():
		self.title = "Error: Quest Not Found"
		self.body = "[Intentional Blank]";

	func _can_start(quest_id: String) -> QuestEntry:
		return null;

	func _is_complete(entry: QuestEntry) -> bool:
		return true;

	func _get_additional_elements(entry: QuestEntry) -> Array[Control]:
		var label := Label.new();
		label.text = "No objective";
		return [];



func is_missing_quest(quest):
	return quest is EmptyQuest;

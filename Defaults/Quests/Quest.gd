extends Panel

@onready var QuestList: VBoxContainer = $VSplitContainer/PanelContainer/TabContainer/Active/ScrollContainer/QuestList
@onready var quest_list_entry: Button = QuestList.get_child(0).duplicate();

@onready var AllQuestList: VBoxContainer = $VSplitContainer/PanelContainer/TabContainer/All/ScrollContainer/QuestList

@onready var DescriptionContainer: VBoxContainer = $VSplitContainer/Description;

@onready var DescriptionTitle: Label = $VSplitContainer/Description/Title;
@onready var DescriptionBody: TextEdit = $VSplitContainer/Description/Body;

@onready var AcceptButtons: HBoxContainer = $VSplitContainer/Description/Buttons;

const QuestEntry = Quests.QuestEntry;
const Quest = Quests.Quest;
var current_quest_id: String = "";

func __reset():
	AcceptButtons.visible = false;
	self.current_quest_id = "";
	var size = DescriptionContainer.get_child_count();
	const default_size: int = 3;
	if size <= default_size:
		return;
	else:
		for x in size - default_size:
			var child = DescriptionContainer.get_child(default_size);
			child.queue_free(); # Is this correct?
			#DescriptionContainer.remove_child(DescriptionContainer.get_child(default_size));

func _display_quest_entry(quest_entry: Quests.QuestEntry):
	__reset();
	var quest := God.quests._get_quest(quest_entry.quest_id) as Quests.Quest;
	DescriptionTitle.text = quest.title;
	DescriptionBody.text = quest.body;

func _display_quest(quest_id: String):
	__reset();
	var quest := God.quests._get_quest(quest_id);
	self.current_quest_id = quest_id;
	DescriptionTitle.text = quest.title;
	DescriptionBody.text = quest.body;

func _open_quest(quest_id: String, display_buttons: bool):
#	printerr("TODO ", "_open_quest");
	_display_quest(quest_id);
	AcceptButtons.visible = display_buttons;

func _take_control(node: Node):
	if node != God.GuiKeys.QUEST_MENU:
		return;
	God.release_mouse();
	self.visible = true;
	set_process_input(true);
	for quest in God.data.active_quests:
		add_quest_entry_to_list(quest);

func add_quest_entry_to_list(quest: Quests.QuestEntry):
	var e := quest_list_entry.duplicate() as Button;
#		e.text = Quests._get_quest(quest.quest_id).title;
	e.text = quest.quest_id;
	e.pressed.connect(func(): self._display_quest_entry(quest));
	QuestList.add_child(e);

func __close():
	__reset();
	set_process_input(false);
	self.visible = false;
	for i in QuestList.get_child_count():
		var child = QuestList.get_child(i);
		child.queue_free(); # Is this correct?
		#QuestList.remove_child(QuestList.get_child(i));

func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("escape") or Input.is_action_just_pressed("quest_meu"):
		get_viewport().set_input_as_handled();
		__close();
		God.return_control_to_player();

func __accept_quest():
	if not self.current_quest_id.is_empty():
		var entry = God.quests._get_quest(current_quest_id)._can_start(current_quest_id);
		if entry:
			God.data.active_quests.append(entry);
			God.data.update_active_quests.emit(PlayerData.Action.add,entry);
		else:
			printerr("TODO: Player cannot start quest");
	AcceptButtons.visible = false;

func __reject_quest():
	__close();

func _update_quest_list(action: PlayerData.Action,quest: Quests.QuestEntry):
	if not self.visible: return;
	match action:
		PlayerData.Action.add:
			add_quest_entry_to_list(quest);
		PlayerData.Action.remove:
			printerr("TODO REmove quest from list");


func _ready() -> void:
	$VSplitContainer/Description/Buttons/Accept.pressed.connect(__accept_quest);
	$VSplitContainer/Description/Buttons/Reject.pressed.connect(__reject_quest);
	#QuestList.remove_child(QuestList.get_child(0));
	var child = QuestList.get_child(0);
	child.queue_free(); # Is this correct?
	God.instance.take_control.connect(_take_control);
	God.data.update_active_quests.connect(_update_quest_list)
	for quest_id in God.quests.quests.keys():
		var quest = God.quests.quests[quest_id];
		var e := quest_list_entry.duplicate() as Button;
		e.text = quest.title;
		e.pressed.connect(func(): self._display_quest(quest_id));
		AllQuestList.add_child(e);
	__close();

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

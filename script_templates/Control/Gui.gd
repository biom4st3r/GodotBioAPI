extends _BASE_

var GuiKey = Object.new(); # Replace with a global key

func _take_control(obj: Object):
	if obj != GuiKey:
		return;
	God.enable_node(self);
	self.visible = true;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	God.instance.take_control.connect(_take_control);
	self.visible = false;
	God.disable_node(self);


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		pass
	if Input.is_action_just_pressed("ui_cancel"):
		pass
	elif Input.is_action_just_pressed("ui_up"):
		pass
	elif Input.is_action_just_pressed("ui_down"):
		pass
	else:
		return;
	self.get_viewport().set_input_as_handled();

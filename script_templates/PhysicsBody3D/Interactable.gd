extends _BASE_

func __interact_with(player):
	printerr("__interact_with not implemented on %s" % [self]);

func __interact_type() -> God.InteractionType:
	return null;

func __view_item() -> ItemStack:
	return ItemStack.EmptyStack;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


class_name PlayerData extends Resource

#signal update_selected(i: int);
signal update_slot(index: int, item: ItemStack);

var inventory: Array[ItemStack] = [];
#var inventory_selected: int = 0

const QuestEntry = Quests.QuestEntry;
var active_quests: Array[Quests.QuestEntry] = [];
var completed_quests: Array[Quests.QuestEntry] = [];
enum Action {add, remove}
var selected_slot: int = 0;

func _get_active_quest_entry(id: String) -> QuestEntry:
	for x in active_quests:
		if x.quest_id == id:
			return x;
	return null

### Returns true if the quest is finished
func update_quest(quest_id: String) -> bool:
	var quest = God.quests._get_quest(quest_id) as Quests.Quest;
	var entry := _get_active_quest_entry(quest_id);
	if not entry:
		printerr("invalid id in update_quest");
		return false;
	quest._update_state(entry);
	if quest._is_complete(entry):
		# TODO Celebrate
		print("Quest Finished!");
		var i = active_quests.find(entry);
		active_quests.remove_at(i);
		completed_quests.append(entry);
		return true;
	return false;

# TODO
signal update_active_quests(action: Action, entry: QuestEntry);

func _init():
#func _ready():
	inventory.resize(10);
	inventory.fill(ItemStack.EmptyStack);

	#update_selected.connect(func(i): self.inventory_selected = i);
	#update_selected.emit(0) # Probably won't work, because this will autoload before anyother scripts

func update_inventory_slot(index: int, item: ItemStack):
	self.update_slot.emit(index, item);

func increment_held_item(amount: int):
	self.increment_slot(self.inventory_selected, amount);

func increment_slot(slot: int, amount: int):
	var stack := (self.get_item(slot) as ItemStack);
	stack.count+=amount;
	self._set_item(slot, stack);
	self.update_slot.emit(slot, stack);

func consume_items(items: Array[ItemStack]) -> bool:
	if items.size() == 0:
		return true;
	var at := PackedInt32Array();

	for needed in items:
		var i = -1;
		var found: bool = false;
		for stack in self.inventory:
			i += 1;
			if stack.item == needed.item and stack.count >= needed.count:
				at.append(i);
				found = true;
				break;
		if not found:
			return false;
	# We have the amounts needed, so to remove them
	for i in items.size():
		increment_slot(at[i], -items[i].count);
	return true;

func get_item(slot: int) -> ItemStack:
	var inv: Array[ItemStack] = self.inventory;
	if inv.size() <= slot:
		printerr("inventory slot to large");
		return ItemStack.EmptyStack;
	if inv[slot].is_empty():
		return ItemStack.EmptyStack;
	return inv[slot];

func _set_item(i: int, item: ItemStack):
	item = item.copy();
	self.inventory[i] = item;
	update_inventory_slot(i, item); # Enforce empty_stack

func add_item(item: ItemStack, caller_ignores_return: bool = true) -> bool:
	if item.is_empty():
		print('tried to add empty item');
		return false;
	for i in self.inventory.size():
		if ItemStack.matches(inventory[i], item):
			inventory[i].count += item.count;
			ToastManager.add_toast(item.get_icon(), "Found x%d %s" % [item.count, item.get_name()]);
			self.update_slot.emit(i, inventory[i]);
			return true;

	for i in inventory.size():
		if inventory[i].is_empty():
			inventory[i] = item;
			ToastManager.add_toast(item.get_icon(), "Found x%d %s" % [item.count, item.get_name()]);
			self.update_inventory_slot(i, inventory[i]);
			return true;
	if caller_ignores_return:
		self.drop_item(item);
	return false;

#func drop_selected_item():
	#if inventory[inventory_selected].is_empty():
		#return;
	#var item = inventory[inventory_selected];
	#inventory[inventory_selected] = ItemStack.EmptyStack;
	#update_inventory_slot(inventory_selected, ItemStack.EmptyStack);
	#drop_item(item);

func drop_item(item: ItemStack):
	if item.is_empty():
		print('tried to drop empty item');
		return;
	God.instance.drop_item.emit(item);
	#var world_item = Items.create_world_item(item);
	#world_item.position = God.player.position + (God.player.transform.basis.rotated(Vector3.UP, 90) * Vector3.ONE) * 2;
	#self.get_parent().add_child(world_item);


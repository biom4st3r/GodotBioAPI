# BioAPI
* Some basics to get a project started
* This directory contains static files used by the defaults
## Data
* Where `Defaults` will look for config files. EG `dialog.json` used by `dialog.gd`
## Default
* Preconfigured modules and systems
## script_templtes
* scripts to automatically plugin some systems

## God - Global singleton with convience functions(and take_control system)
* class Interact - interface for Interaction
* take\_control - signal for passing control between guis and player
* class GuiKeys - Single source of truth for take_control keys
* return\_control\_to\_player - wrapper for take\_control
* open_*gui - wrapper for take\_control
* enable\_node() - stops a node from processing. Convienent for guis
* disable\_node()
* grab\_mouse() - Window controls mouse
* release\_mouse() - Window releases mouse
* remove\_all\_children() - Good for gui lists

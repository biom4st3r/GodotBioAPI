class_name God extends Object

const TEXT_SPEED = 75;
var _dialog: Dictionary;

func _init():
	var file = FileAccess.open("res://addons/BioAPI/data/dialogs.json", FileAccess.READ);
	_dialog = JSON.parse_string(file.get_as_text());

enum InteractionType {
	Default,
	Custom,
	Dialog,
	Item,
}

static func disable_node(node: Node) -> void:
	node.set_process_input(false);
	node.set_process_unhandled_input(false);
	node.set_process(false);
	node.set_physics_process(false);

static func enable_node(node: Node) -> void:
	node.set_process_input(true);
	node.set_process_unhandled_input(true);
	node.set_process(true);
	node.set_physics_process(true);

class Interact:
	static func get_item(node: Node3D) -> ItemStack:
		return node.__view_item() as ItemStack;
	static func get_type(node: Node3D) -> InteractionType:
		return node.call("__interact_type");
	#static func can_interact(node: Node3D) -> bool:
		#return node.has_method("__interact_with");
	static func interact_with(node: Node3D, player):
		return node.call("__interact_with", player);
	static func try_interact(player, ray: RayCast3D):
		if ray.collision_mask & 1 << 31 == 0:
			printerr("Raycast3d used for interaction doesn't have the proper collision mask(32)");
			return;
		if ray.collision_mask & 1 != 0:
			printerr("Raycast3d uses collison layer 1. This is probably a bug.");
		var collider = ray.get_collider();
		if collider: # and Interact.can_interact(collider):
			interact_with(collider,player);
		else:
			print("No hit");

## This is called by node scene that wish to take control on user input
## node: the node taking control or null to return to the player
signal take_control(node: Object, data: Variant);
static func return_control_to_player():
	God.instance.take_control.emit(null, null);

static func release_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE;

static func grab_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED;

static func remove_all_children(node: Node):
	for x in node.get_children():
		x.queue_free();

static func is_scroll_up(event: InputEvent):
	var val = event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_UP
	return val and event.is_pressed();
static func is_scroll_down(event: InputEvent):
	var val = event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_DOWN
	return val and event.is_pressed();

# Only neede when add a child from a tool script
static func tool_add_child(parent: Node, child: Node):
	parent.add_child(child, true);
	child.owner = parent.owner;

# PlayerData
static var data: PlayerData = PlayerData.new();
static var instance = God.new();
static var quests: Quests = Quests.new();

# GUI
class GuiKeys:
	static var QUEST_MENU = Object.new();
	static var INVENTORY_MENU = Object.new();
	static var DIALOG_BOX = Object.new();
	static var SHOP = Object.new();

static func open_inventory():
	God.instance.take_control.emit(God.GuiKeys.INVENTORY_MENU, null);

static func get_item(id: String) -> Variant:
	return Items.items.get(id, ItemStack.MissingItem);

signal drop_item(item: ItemStack);

# Quests
signal open_quest_id(quest_id: String, show_accept_buttons: bool);

static func open_quest_menu():
	God.instance.take_control.emit(God.GuiKeys.QUEST_MENU, null);

static func open_quest(quest_id: String, show_accept_buttons: bool):
	open_quest_menu();
	God.instance.open_quest_id.emit(quest_id, show_accept_buttons);

static func open_dialog(id: String):
	var a: Array = God.instance._dialog.get(id);
	God.instance.take_control.emit(GuiKeys.DIALOG_BOX, a);

static func open_shop(settings: Dictionary):
	God.instance.take_control.emit(God.GuiKeys.SHOP, settings);

